#!/usr/bin/env bash
#// C++：gcc -shared -I /usr/lib/jvm/java-7-openjdk-amd64/include -fPIC test.cpp -o libjni-test.so
#// C:gcc -shared -I /usr/lib/jvm/java-7-openjdk-amd64/include -fPIC test.c -o libjni-test.so
#//上面的语句中，/usr/lib/jvm/java-7-openjdk-amd64是本地的JDK安装路径，在其他环境编译时也可以将其指向本机的jdk路径即可
#//而libjni-test.so则是生成so库的名字，在JAVA中可以通过如下方式加载System.loadLibaray(“jni-test”)
#//其中so库名字中的“lib”和“.so”是不需要明确指明的，so编译后，就可以调用了
#g++-7 -I./ -I/usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -fPIC -shared -o libjdetection.so runtime_detection_Detector.cpp -L/root/map123/app/app1/project2_dynamiclinklib/detection/1018CPUGPUBSV6/test_java_detector_v3.1 -L/usr/local/$v_cuda/lib64 -ldetection
g++-7 -shared -I /usr/lib/jvm/java-8-openjdk-amd64/include/linux -fPIC test.cpp -o libjni-test.so
gcc -shared -I /usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux -fPIC test.cpp -o libjni-test.so