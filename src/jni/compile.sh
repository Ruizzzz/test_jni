export JAVA_INC_PATH=$JAVA_HOME/include/
#export JAVA_LINUX_INC_PATH=$JAVA_HOME/include/darwin
export JAVA_LINUX_INC_PATH=$JAVA_HOME/include/linux
export TF_INC_PATH=/usr/local/include/tf
export EIGEN_INC_PATH=/usr/local/lib/python3.6/dist-packages/tensorflow_core/include
export CV_INC_PATH=/usr/local/include/opencv
export TF_LIB_PATH=/usr/local/lib

export Tensorrt_path=/usr/include/x86_64-linux-gnu/
export Tensorrt_path_lib=/usr/lib/x86_64-linux-gnu/
export cuda_path=/usr/local/cuda/include
export cuda_lib=/usr/local/cuda/lib64

# nvcc  -gencode arch=compute_61,code=sm_61 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_50,code=[sm_50,compute_50] -gencode arch=compute_52,code=[sm_52,compute_52] -Iinclude/ -Isrc/  -DGPU -I/usr/local/cuda/include/ -DCUDNN  --compiler-options "-Wall -Wno-unused-result -Wno-unknown-pragmas -Wfatal-errors -fPIC  -fopenmp -Ofast  -DGPU -DCUDNN" -c GPUHello.cu -o GPUHello.o

# ar rcs GPUHello.a GPUHello.o

# g++ Detection.cpp -I./ -I$JAVA_INC_PATH -I$JAVA_LINUX_INC_PATH \
# -L$JAVA_HOME/lib -L./  -L/usr/local/cuda-10.0/lib64 -lcudart -lcuda -lcublas -lcurand -lcudnn GPUHello.a \
# -fPIC -shared -o ./libdetection.so


# tf compile
g++ yolov3.cpp calibrator.cpp  -I$JAVA_INC_PATH -I$JAVA_LINUX_INC_PATH \
-I$TF_INC_PATH \
-I$EIGEN_INC_PATH -I$CV_INC_PATH \
-I$Tensorrt_path -I$cuda_path \
-L$Tensorrt_path_lib -L$cuda_lib \
-L$TF_LIB_PATH \
-lnvinfer -lcudart -lyololayer \
`pkg-config opencv --cflags --libs` -ltensorflow_cc -ltensorflow_framework \
-g -fPIC -shared -o ./libjpic.so
