import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.System;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


public class test {

    private static List<String> labels;
    static byte[] bytes;


    static {
//        System.loadLibrary("jtrtdetection");
        System.loadLibrary("jpic");
    }
    public static void main(String[] args) throws Exception {
        String picture = "/root/map123/mlr/dog.jpg";

//        result = inference(picture);

        File file = new File(picture);
//        BufferedImage bi = ImageIO.read(file);
//        int width = image.getWidth();
//        int height = image.getHeight();
//        System.out.println(width);
//        System.out.println(height);
        byte[] bytepicture = fileToByte(file);
//
        ByteArrayInputStream in = new ByteArrayInputStream(bytepicture); //将b作为输入流；
        BufferedImage bi = ImageIO.read(in);
        //将in作为输入流，读取图片存入image中，而这里in可以为ByteArrayInputStream();


        int width = bi.getWidth();
        int height = bi.getHeight();
        int minx = bi.getMinX();
        int miny = bi.getMinY();
        int length = (width - minx) * (height - miny);
        byte[] r = new byte[length];
        byte[] g = new byte[length];
        byte[] b = new byte[length];
        int size = 0;
        for (int j = miny; j < height; j++) {
            for (int i = minx; i < width; i++) {
                int pixel = bi.getRGB(i, j);
                b[size] = (byte)((pixel & 0xff0000) >> 16);
                g[size]= (byte)((pixel & 0xff00) >> 8);
                r[size] = (byte)(pixel & 0xff);
                size++;
            }
        }

//        String s = getIdByByte(r,g,b,height,width);
//        System.out.println(s);
        init();
        float result[][] = new float[20][20];

        result = inference(r,g,b,height,width);
        System.out.println("load success");
        int col = result.length;
        System.out.println(result.length);
        System.out.println(result[0].length);

        labels = Files.readAllLines(Paths.get("/root/map123/app/app1/yolo/coco.names"));
        System.out.println("read labels success!");

        for (int j=0;j<col;j++){
            for (int i=0;i<5;i++){
                System.out.println(result[j][i]);
            }
            String name = labels.get(Double.valueOf(result[j][0]).intValue());
            System.out.println(name);
        }
        destroyall();



    }
    public static native void init();
    public static native float[][] inference(byte[] r, byte[] g, byte[] b, int width, int height);
    public static native String getIdByByte(byte[] r,byte[] g,byte[] b, int width, int height);
    public static native void destroyall();

    public static byte[] fileToByte(File img) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            BufferedImage bi;
            bi = ImageIO.read(img);
            ImageIO.write(bi, "jpg", baos);
            bytes = baos.toByteArray();
            System.err.println(bytes.length);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            baos.close();
        }
        return bytes;
    }
}
