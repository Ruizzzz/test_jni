import os
os.environ["CUDA_VISIBLE_DEVICES"] = "1"
os.environ["PYSPARK_PYTHON"] = "/usr/bin/python3"
os.environ["PYSPARK_DRIVER_PYTHON"] = "/usr/bin/python3"
import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import pandas as pd
import numpy as np
import time

from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession
from pyspark.sql.functions import pandas_udf, PandasUDFType
from pyspark.sql.types import *
from pyspark import StorageLevel
from typing import Iterator, Tuple
from pyspark.sql.functions import udf, col
from pyspark.sql.types import StringType
from common import HostDeviceMem

from common import HostDeviceMem
import cv2
import pycuda.driver as cuda
import pycuda.autoinit

width=608
height=608
batchsize=1
img_size=width*height


# build TensorRT engine
MODEL_PATH = "/root/map123/mlr/yolov3_608.caffemodel"
DEPLOY_PATH = "/root/map123/mlr/yolov3_608.prototxt"
DTYPE = trt.float16
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
trt.init_libnvinfer_plugins(TRT_LOGGER, '')
builder = trt.Builder(TRT_LOGGER)
network = builder.create_network()
config = builder.create_builder_config()
parser = trt.CaffeParser()
config.max_workspace_size = 10*(2<<20)
builder.max_batch_size = batchsize

model_tensors = parser.parse(deploy=DEPLOY_PATH, model=MODEL_PATH, network=network, dtype=DTYPE)
# network.mark_output(model_tensors.find("bbox_pred"))
# network.mark_output(model_tensors.find("cls_prob"))
# network.mark_output(model_tensors.find("rois"))
network.mark_output(model_tensors.find("layer84-route"))
network.mark_output(model_tensors.find("layer87-route"))
network.mark_output(model_tensors.find("layer96-route"))
network.mark_output(model_tensors.find("layer99-route"))
engine = builder.build_engine(network, config)
context = engine.create_execution_context()

inputs = []
outputs = []
bindings = []
stream = cuda.Stream()

for binding in engine:
    print(binding)
    size = trt.volume(engine.get_binding_shape(binding)) * engine.max_batch_size
    dtype =  trt.nptype(engine.get_binding_dtype(binding))
    host_mem = cuda.pagelocked_empty(size, dtype)
    device_mem = cuda.mem_alloc(host_mem.nbytes)
    bindings.append(int(device_mem))
    if engine.binding_is_input(binding):
        inputs.append(HostDeviceMem(host_mem, device_mem))
    else:
        outputs.append(HostDeviceMem(host_mem, device_mem))
#

# preall = 0
# tcopyall = 0
# predictall = 0
#
# for batch in batch_iter:
#     tprebatch = 0
#
#     # data copy
#     tcopyb = time.time()
#     raw_input = np.fromstring(batch, dtype=np.uint8).copy().reshape((height, width, 3))
#     tcopyd = time.time()
#     tcopy = tcopyd - tcopyb
#     tcopyall = tcopyall + tcopy
#
#     # # preprocess
#     # im_data = np.array([])
#     # for b in range(batchsize):
#     #     pre_start_time =time.time()
#     #     model_input_width = 500
#     #     model_input_height = 375
#     #     model_input_c = 3
#     #     image_resized = cv2.resize(raw_input,(model_input_width,model_input_height))
#     #     img_np = np.array(image_resized).reshape((model_input_height, model_input_width, model_input_c)).astype(np.uint8)
#     #     img_np = img_np.transpose((2, 0, 1))
#     #     img_np = (2.0 / 255.0) * img_np - 1.0
#     #     img_np = img_np.ravel()
#     #     im_data = np.append(im_data, img_np)
#     #     pre_end_time = time.time()
#     #     tprebatch = tprebatch + pre_end_time - pre_start_time
#     # preall = preall + tprebatch
#
#     # preprocess
#     im_data = np.array([])
#     pre_start_time = time.time()
#     model_input_width = 608
#     model_input_height = 608
#     model_input_c = 3
#     image_resized = cv2.resize(raw_input, (model_input_width, model_input_height))
#     img_np = np.array(image_resized).reshape((model_input_height, model_input_width, model_input_c)).astype(np.uint8)
#     img_np = img_np.transpose((2, 0, 1))
#     img_np = (2.0 / 255.0) * img_np - 1.0
#     img_np = img_np.ravel()
#     im_data = np.append(im_data, img_np)
#     pre_end_time = time.time()
#     preall = preall + pre_end_time - pre_start_time
#
#
#     np.copyto(inputs[0].host, im_data)
#     im_info = [model_input_height, model_input_width, 1]
#     np.copyto(inputs[1].host, np.tile(im_info, batchsize))
#
#     # predict
#     inference_start_time = time.time()
#     [cuda.memcpy_htod_async(inp.device, inp.host, stream) for inp in inputs]
#     context.execute_async(batch_size=batchsize, bindings=bindings, stream_handle=stream.handle)
#     [cuda.memcpy_dtoh_async(out.host, out.device, stream) for out in outputs]
#     stream.synchronize()
#     [bbox_pred, cls_prob, rois] = [out.host for out in outputs]
#     inference_end_time = time.time()
#     predictall = predictall + inference_end_time - inference_start_time
#
# print("avg copy time: " + str(tcopyall * 1000.0 / len(batch_iter)) + " ms")
# print("avg preprocess time :" + str(preall * 1000.0 / len(batch_iter)) + " ms")
# print("predict avg time: " + str((predictall * 1000.0) / len(batch_iter)) + " ms")
# result = [i for i in range(0, len(batch_iter))]






















